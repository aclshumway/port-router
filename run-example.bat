:: start servers in background
start /b node ./fileserver.js 127.0.0.1:4000 ../ns/crm less jade-index
start /b node ./fileserver.js 127.0.0.1:4001 ../ns/crm/www
::start /b node ./fileserver.js 127.0.0.1:2000 ../gps-client less

:: start port router
node ./router.js ./routes.json 8080

::@NOTE: doesn't wait until all background servers have finished,
:: but closing the command prompt window will stop them.
