if (!process.argv[2]) {
  throw new Error('missing port');
}
if (!process.argv[3]) {
  throw new Error('missing dir');
}

var http = require('http'),
  url = require('url'),
  path = require('path'),
  fs = require('fs'),
  mime = require('mime'),
  hostAndPort = process.argv[2],
  ray = hostAndPort.split(':'),
  hostname, port,
  folder = path.join(__dirname, process.argv[3] || ''),
  exec = require('child_process').exec,
  validFlags = {
    'less': true,
    'jade-index': true,
    'jade-all': true,
  },
  useFlagsRegx = /index.html$/,
  flags = [],
  formatRegex = /\{([0-9]+)\}/g,
  logPrefix, silent,
  isWindows = process.platform === 'win32';
if (isWindows) {
  console.log('WINDOWS: using windows checks');
}

// make it a boolean
silent = !!process.env.SILENT;

if (ray.length === 1) {
  hostname = '127.0.0.1';
  port = ray[0];
} else {
  hostname = ray[0];
  port = ray[1];
}
logPrefix = '[' + hostname + ':' + port + ']';

function aformat(format, argsArray) {
  return (format || '').replace(formatRegex, function(item, match) {
    var val = argsArray[match];
    if (val != null) {
      return val;
    } else {
      return "";
    }
  });
}

process.argv.slice(4).forEach(function(arg) {
  if (!validFlags[arg]) {
    throw new Error(aformat('invalid flag {0}', [arg]));
  }
  if (arg === 'jade-all') {
    flags.push('jade-tmpls');
    flags.push('jade-index');
  } else {
    flags.push(arg);
  }
});

function readFilepaths(dir, list) {
  fs.readdirSync(dir).forEach(function(name) {
    // exlude files starting with an underscore
    if (name[0] === '_') {
      return;
    }

    var filepath = path.join(dir, name);
    if (fs.statSync(filepath).isDirectory()) {
      readFilepaths(filepath, list);
    } else {
      list.push(filepath);
    }
  });
  return list;
}

function generateAllTemplatesFile(folder, next) {
  try {
    folder = path.join(folder, 'jade');

    // read filepaths
    var filepaths = readFilepaths(folder, []),
      output = [];

    // generate file
    filepaths.forEach(function(filepath) {
      // for id, remove file extension and folder
      var id = path.basename(filepath, path.extname(filepath));
      id = '#tmpl-' + id.replace(/\./g, '_');
      output.push(id + '\n  include ./' + path.relative(folder, filepath) + '\n');
    });

    // write file
    fs.writeFileSync(path.join(folder, '_all.jade'), output.join(''));
  } catch (ex) {
    next(ex);
    return;
  }
  next();
}


function trim(text) {
  if (text) {
    text = (text + '').replace(/^\s+|\s+$/g, '');
  } else if (text !== '') {
    text = null;
  }
  return text;
}

function runExec(cmd, next) {
  console.log('runExec:', cmd);
  exec(cmd, function(err) {
    if (err && isWindows) {
      // windows doesn't like my test for file existance
      // so check the error message
      var errMsg = trim(err.message);
      if (errMsg === 'Command failed:' || errMsg.substr(0, 17) === 'Command failed: \'') {
        console.log('runExec err nulled');
        err = null;
      }
    }
    next(err);
  });
}

function processFlags(filepath, cb) {
  var remainingFlags = flags.slice();

  console.log(logPrefix, '--using flags');
  (function next(err) {
    if (err) {
      console.log(logPrefix, 'err:', err);
      return cb(err);
    }

    if (!remainingFlags.length) {
      console.log(logPrefix, '--done using flags');
      cb();
    } else {
      var flag = remainingFlags.shift(),
        cmd;
      console.log(logPrefix, '-- flag:', flag);
      switch (flag) {
        case 'less':
          // compile less file if it exists
          cmd = '{0} --no-color {1} > {2}';
          if (!isWindows) {
            // test that file exists
            cmd = '[ ! -f {1} ] || ' + cmd;
          }
          cmd = aformat(cmd, [path.join(folder, 'node_modules/.bin/lessc'), path.join(path.dirname(filepath), 'index.less'), path.join(path.dirname(filepath), 'index.css')]);
          runExec(cmd, next);
          break;
        case 'jade-tmpls':
          generateAllTemplatesFile(folder, next);
          break;
        case 'jade-index':
          cmd = aformat('{0} {1}', [path.join(folder, 'node_modules/.bin/jade'), path.join(path.dirname(filepath), 'index.jade')]);
          runExec(cmd, next);
          break;
      }
    }
  })();
}

function writeErr(uri, err, response) {
  console.log(logPrefix, 'err', uri, ':', err);
  response.writeHead(500, {
    'Content-Type': 'text/plain'
  });
  response.write(err + '\n');
  response.end();
}

function writeFile(uri, filename, response) {
  fs.readFile(filename, 'binary', function(err, file) {
    if (err) {
      writeErr(uri, err, response);
    } else {
      response.writeHead(200, {
        'Content-Type': mime.lookup(filename)
      });
      response.write(file, 'binary');
      response.end();
      // console.log(logPrefix,'end', uri);
    }
  });
}

var server = http.createServer(function(request, response) {
  var uri = url.parse(request.url).pathname,
    filepath = path.join(folder, uri);

  if (!silent) {
    console.log(logPrefix, filepath);
  }

  fs.exists(filepath, function(exists) {
    if (!exists) {
      // console.log(logPrefix,'!exist', uri);
      response.writeHead(404, {
        'Content-Type': 'text/plain'
      });
      response.write('404 Not Found\n');
      response.end();
      return;
    }

    if (fs.statSync(filepath).isDirectory()) {
      if (filepath[filepath.length - 1] !== '/') {
        console.log(logPrefix, 'missing slash:', filepath);
        filepath += '/';
      }
      filepath += 'index.html';
    }

    if (useFlagsRegx.test(filepath)) {
      processFlags(filepath, function(err) {
        if (err) {
          writeErr(uri, err, response);
        } else {
          writeFile(uri, filepath, response);
        }
      });
    } else {
      writeFile(uri, filepath, response);
    }
  });
});
server.on('error', function(err) {
  console.error('***********************************\nError on', hostname + ':' + port, err, '\n***********************************');
});
server.listen(parseInt(port, 10), hostname, function() {
  console.log(logPrefix, 'running at => http://' + hostname + ':' + port, process.argv.slice(4));
});
