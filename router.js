#!/usr/bin/env node

var argv = require('optimist').argv,
  configFile = argv.config || argv._.shift(),
  port = parseInt(argv.port || argv._.shift(), 10),
  address = argv.address || argv._.shift() || '127.0.0.1',
  sslport = argv.sslport || 0,
  fs = require('fs'),
  routesConfig = require('./routesConfig.js'),
  path = require('path'),
  url = require('url'),
  crypto = require('crypto'),
  configJson,
  logPrefix = '[proxy]';

if (!configFile || !port) {
  fs.createReadStream(__dirname + '/routerusage.txt')
    .pipe(process.stdout)
    .on('end', process.exit.bind(null, 1));
  return;
}

function tryReadFileSync(path) {
  try {
    return fs.readFileSync(path);
  } catch (ex) {
    console.error("******ERROR READING FILE******", ex);
    console.error(ex.stack);
  }
}

function getSslOptions(ssl) {
  var options = {};
  if (ssl.pfx) {
    options.pfx = tryReadFileSync(ssl.pfx);
  }
  if (ssl.key) {
    options.key = tryReadFileSync(ssl.key);
  }
  if (ssl.cert) {
    options.cert = tryReadFileSync(ssl.cert);
  }
  if (ssl.ca) {
    options.cs = (Array.isArray(ssl.ca) ? ssl.ca.map(function(path) {
      return tryReadFileSync(path);
    }) : tryReadFileSync(ssl.ca));
  }
  // console.log("options", options);
  return options;
}

configJson = routesConfig.parseEndpoints(path.join(__dirname, configFile));
var keys = Object.keys(configJson);
var sslOptionsMap = {};
// print routes
keys.forEach(function(key) {
  var endpoint = configJson[key];
  if (sslport && endpoint.ssl) {
    sslOptionsMap[key] = getSslOptions(endpoint.ssl);
    key = "ssl " + key;
  }
  console.log(logPrefix, 'proxy:', key, '--->', routesConfig.stringifyEndpoint(endpoint));
});

function getRequestPath(req) {
    var host = req.headers.host,
      index = host.indexOf(':');
    // remove port
    if (index >= 0) {
      host = host.substr(0, index);
    }
    return host + req.url;
  }
  //
function getEndpoint(req) {
  var requestPath = getRequestPath(req),
    route, relativePath;
  keys.some(function(k) {
    if (requestPath.substr(0, k.length) === k && configJson[k]) {
      route = configJson[k];
      relativePath = requestPath.substr(k.length);
      return true;
    }
  });
  if (!route) {
    console.warn(logPrefix, 'no route for', req.headers.host + req.url);
    return null;
  }
  // make into object
  return {
    log: route.log,
    target: {
      host: route.host,
      port: route.port,
    },
    path: url.resolve(route.path, relativePath),
  };
}

function logProxy(endpoint, req) {
  if (endpoint.log) {
    console.log(logPrefix, req.method, req.headers.host + req.url,
      '\n ------to->', endpoint.target.host + ":" + endpoint.target.port + endpoint.path);
  }
}

var httpProxy = require('http-proxy');
var proxy = new httpProxy.createProxyServer({
  ws: true,
});
proxy.on('error', function(err, req, res) {
  res.statusCode = 500;
  res.end(JSON.stringify({
    Message: 'Proxy ' + err,
  }));
});

function requestListener(req, res) {
  try {
    var endpoint = getEndpoint(req);
    if (!endpoint) {
      res.statusCode = 400;
      res.end('{"Message":"Failed to proxy request. No target endpoint."}');
    } else {
      logProxy(endpoint, req);
      req.headers.host = endpoint.target.host;
      req.url = endpoint.path;
      proxy.web(req, res, {
        target: endpoint.target,
      }, function(err) {
        res.statusCode = 500;
        res.end(JSON.stringify({
          Message: 'Proxy ' + err,
        }));
      });
    }
  } catch (ex) {
    console.error(logPrefix, 'http server ex:', ex);
    res.statusCode = 500;
    res.end(JSON.stringify({
      Message: 'Http Proxy ' + ex,
    }));
  }
}

function onUpgrade(req, socket, head) {
  try {
    console.log(logPrefix, 'websocket upgrade');
    var endpoint = getEndpoint(req);
    if (!endpoint) {
      socket.end();
    } else {
      logProxy(endpoint, req);
      req.headers.host = endpoint.target.host;
      req.url = endpoint.path;
      proxy.ws(req, socket, head, {
        target: endpoint.target,
      }, function(err) {
        console.error(logPrefix, 'ws server error:', err);
        socket.end();
      });
    }
  } catch (ex) {
    console.error(logPrefix, 'ws server ex:', ex);
    socket.end();
  }
}

function startServer(proxyServer, listenPort) {
  // Listen to the `upgrade` event and proxy the WebSocket requests as well.
  proxyServer.on('upgrade', onUpgrade);
  proxyServer.on('error', function onError(err) {
    console.error(logPrefix, '***********************************\nError on', address + ':' + listenPort, err, '\n***********************************');
  });
  proxyServer.listen(listenPort, address, function onListen() {
    console.log(logPrefix, 'listening on:', ((listenPort === sslport) ? "https://" : "http://") + address + ':' + listenPort);
  });

}


function createHttpServer() {
  return require('http').createServer(requestListener);
}

function createHttpsServer(sslOptionsMap) {
  var sslOptions = {};
  var sslDomains = Object.keys(sslOptionsMap);
  sslDomains.forEach(function(k) {
    try {
      sslOptions[k] = crypto.createCredentials(sslOptionsMap[k]).context;
    } catch (ex) {
      console.error("******ERROR CREATING CREDENTIALS******", ex);
      console.error(ex.stack);
    }
  });
  return require('https').createServer({
    SNICallback: function(domain) {
      var results = sslOptions[domain];
      if (!results) {
        console.warn("SNICallback no ssl certs for domain:", domain);
        console.log("available domains:", sslDomains);
      }
      return results;
    },
    key: sslOptionsMap[sslDomains[0]].key,
    cert: sslOptionsMap[sslDomains[0]].cert,
  }, requestListener);
}


startServer(createHttpServer(sslOptionsMap), port);
if (Object.keys(sslOptionsMap).length) {
  console.log("HTTPS");
  startServer(createHttpsServer(sslOptionsMap), sslport);
}
