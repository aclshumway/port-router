package main

import (
	"log"
	"mime"
	"net/http"
)

//
//
//
//
// @NOTE: currently does NOT work like fileserver.js
//
//
//
//

func main() {
	// set correct mime type for javascript files
	mime.AddExtensionType(".js", "application/javascript")

	log.Println("Listening on 7777")
	http.Handle("/", http.StripPrefix("/", &serverWrapper{http.FileServer(http.Dir("./"))}))
	if err := http.ListenAndServe(":7777", nil); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

type serverWrapper struct {
	handler http.Handler
}

func (s *serverWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	upath := r.URL.Path
	log.Printf("path: %s\n", upath)
	s.handler.ServeHTTP(w, r)
}
