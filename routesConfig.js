var fs = require('fs'),
  url = require('url');

function createEndpoint(val) {
  // presumed format: (ip|domain):port/path
  // e.g. '127.0.0.1:12345/path/goes/here/'
  var obj = url.parse('http://' + val);
  return {
    host: obj.hostname,
    port: obj.port || 80,
    path: obj.pathname,
  };
}

function fixEndpoint(val) {
  val.host = val.host || '127.0.0.1';
  val.port = parseInt(val.port, 10) || 80;
  val.path = val.path || '/';
}

exports.stringifyEndpoint = function(endpoint) {
  // return 'http://' + endpoint.host + ':' + endpoint.port + endpoint.path;
  return endpoint.host + ':' + endpoint.port + endpoint.path;
};

exports.parseEndpoints = function(configPath) {
  var configJson;
  // relaxed json parsing
  eval('configJson=' + fs.readFileSync(configPath, 'utf8'));
  Object.keys(configJson).forEach(function(key) {
    "use strict";
    var val = configJson[key];
    if (typeof(val) === 'number') {
      configJson[key] = {
        host: '127.0.0.1',
        port: val,
        path: '/',
      };
    } else if (typeof(val) !== 'object') {
      configJson[key] = createEndpoint(val);
    } else {
      fixEndpoint(val);
    }
  });
  return configJson;
};
