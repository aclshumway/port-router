#!/bin/bash
set -e
set -u

# kill sub-processes on these signals (1-SIGHUP, 2-SIGINT, 9-SIGKILL, 15-SIGTERM)
trap 'kill $(jobs -pr)' 1 2 9 15

# bind to port 80 without root access by running these commands:
# found here: http://technosophos.com/2012/12/17/run-nodejs-apps-low-ports-without-running-root.html
#sudo apt-get install libcap2-bin
#sudo setcap 'cap_net_bind_service=+ep' /usr/local/bin/node

# export SILENT=1

#
# start servers
#
node ./fileserver.js 127.0.0.1:4000 ../ns/crm less jade-index &
node ./fileserver.js 127.0.0.1:4001 ../ns/crm/www &
# node ./fileserver.js 127.0.0.1:2000 ../ns/gps less &


#
# start router
#
./router.js ./routes.json 8080 &
# don't close until all background processes are finished
wait
