Port Router
===========

This is a node.js server that makes it easier to develop wep apps that have less and jade files.
Before serving index.html it compiles less and jade files associated with the requested index.html file.
It also has the added benefit of telling the web browser to never cache.


Dependencies
------------

  - [Node.js](http://nodejs.org/), plus node module dependencies defined in package.json



Setup
-----

Copy example config file and run script. Then modify them to fit your environment.

  - Linux:

        cp routes-example.json routes.json
        cp run-example.sh run.sh

  - Windows:

        copy routes-example.json routes.json
        copy run-example.bat run.bat


Run
---

  Execute the run script

  - Linux:

        ./run.sh

  - Windows:

      The best way is to execute run.bat from File Explorer, which will open a command prompt to execute run.bat.
      Stopping run.bat in the command prompt doesn't stop all the started servers, but closing the command prompt does.

